/**
 * 
 */
package org.tio.core;

import org.tio.core.intf.Packet;

/**
 * @author tanyaowu
 *
 */
public interface PacketConverter {
	/**
	 * 
	 * @param packet
	 * @param channelContext
	 * @return
	 * @author tanyaowu
	 */
	public Packet convert(Packet packet, ChannelContext channelContext);
}
