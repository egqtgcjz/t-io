package org.tio.utils.cache;

import java.io.Serializable;

import org.tio.utils.cache.caffeine.CaffeineCache;
import org.tio.utils.lock.LockUtils;

/**
 * @author tanyaowu
 *
 */
public class CacheUtils {

	private static final String prefix_timeToLiveSeconds = CacheUtils.class.getName() + "_live";

	private static final String prefix_timeToIdleSeconds = CacheUtils.class.getName() + "_idle";
	
	private static final Object lockForGetcache = new Object();

	private CacheUtils() {
	}

	/**
	 * 
	 */
	private static final Serializable NULL_OBJ = new Serializable() {
		private static final long serialVersionUID = 3360149895681472163L;
	};

	/**
	 * 
	 * @param timeToLiveSeconds
	 * @param timeToIdleSeconds
	 * @param cacheKey
	 * @param firsthandCreater
	 * @return
	 * @author tanyaowu
	 */
	public static <T extends Serializable> T get(Long timeToLiveSeconds, Long timeToIdleSeconds, String cacheKey, FirsthandCreater<T> firsthandCreater) {
		return get(timeToLiveSeconds, timeToIdleSeconds, cacheKey, false, firsthandCreater);
	}

	/**
	 * timeToLiveSeconds和timeToIdleSeconds一个传null一个传值
	 * @param timeToLiveSeconds
	 * @param timeToIdleSeconds
	 * @param cacheKey 请业务侧保证cacheKey的唯一性，建议的做法是由prefix + key组成，譬如"user.124578"，其中user就是prefix，124578就是key
	 * @param useTempIfNull true:可以防止缓存null攻击
	 * @param firsthandCreater
	 * 
	 * @return
	 * @author tanyaowu
	 */
	public static <T extends Serializable> T get(Long timeToLiveSeconds, Long timeToIdleSeconds, String cacheKey, boolean useTempIfNull, FirsthandCreater<T> firsthandCreater) {
		CaffeineCache cache = getCache(timeToLiveSeconds, timeToIdleSeconds);
		return get(cache, cacheKey, useTempIfNull, firsthandCreater);
	}

	/**
	 * 
	 * @param cache
	 * @param cacheKey
	 * @param firsthandCreater
	 * @return
	 * @author tanyaowu
	 */
	public static <T extends Serializable> T get(ICache cache, String cacheKey, FirsthandCreater<T> firsthandCreater) {
		return get(cache, cacheKey, false, firsthandCreater);
	}

	/**
	 * 
	 * @param cache
	 * @param cacheKey
	 * @param useTempIfNull
	 * @param firsthandCreater
	 * @return
	 * @author tanyaowu
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T get(ICache cache, String cacheKey, boolean useTempIfNull, FirsthandCreater<T> firsthandCreater) {
		Serializable ret = cache.get(cacheKey);
		if (ret != null) {
			if (NULL_OBJ == ret) {
				return null;
			}
			return (T) ret;
		}
		Object lock = LockUtils.getLockObj(cacheKey, cache);

		synchronized (lock) {
			ret = cache.get(cacheKey);
			if (ret != null) {
				if (NULL_OBJ == ret) {
					return null;
				}
				return (T) ret;
			}

			ret = firsthandCreater.create();
			if (ret == null) {
				if (useTempIfNull) {
					cache.putTemporary(cacheKey, NULL_OBJ);
				}
			} else {
				cache.put(cacheKey, ret);
			}
		}

		return (T) ret;
	}

	/**
	 * 
	 * @param timeToLiveSeconds
	 * @param timeToIdleSeconds
	 * @return
	 * @author tanyaowu
	 */
	private static CaffeineCache getCache(Long timeToLiveSeconds, Long timeToIdleSeconds) {
		String cacheName = getCacheName(timeToLiveSeconds, timeToIdleSeconds);
		CaffeineCache caffeineCache = CaffeineCache.getCache(cacheName, true);
		if (caffeineCache == null) {
			synchronized (lockForGetcache) {
				caffeineCache = CaffeineCache.getCache(cacheName, true);
				if (caffeineCache == null) {
					caffeineCache = CaffeineCache.register(cacheName, timeToLiveSeconds, timeToIdleSeconds);
				}
			}
		}

		return caffeineCache;
	}

	private static String getCacheName(Long timeToLiveSeconds, Long timeToIdleSeconds) {
		if (timeToLiveSeconds != null) {
			return prefix_timeToLiveSeconds + timeToLiveSeconds;
		} else if (timeToIdleSeconds != null) {
			return prefix_timeToIdleSeconds + timeToIdleSeconds;
		} else {
			throw new RuntimeException("timeToLiveSeconds和timeToIdleSeconds不允许同时为空");
		}
	}

	public static void main(String[] args) {
	}

}
