package org.tio.utils.lock;

import java.io.Serializable;

import org.tio.utils.cache.caffeine.CaffeineCache;

/**
 * 锁对象工具类
 */
public class LockUtils {
	private static final CaffeineCache LOCAL_LOCKS = CaffeineCache.register(LockUtils.class.getName(), null, 3600L);

	/**
	 * 获取锁对象，用于synchronized(lockObj)
	 * @param key
	 * @return
	 * @author tanyaowu
	 */
	public static Serializable getLockObj(String key) {
		return getLockObj(key, null);
	}

	/**
	 * 获取锁对象，用于synchronized(lockObj)
	 * @param key
	 * @param myLock
	 * @return
	 * @author tanyaowu
	 */
	public static Serializable getLockObj(String key, Object myLock) {
		Serializable lock = LOCAL_LOCKS.get(key);
		if (lock == null) {
			Object ml = myLock;
			if (ml == null) {
				ml = LockUtils.class;
			}
			synchronized (ml) {
				lock = LOCAL_LOCKS.get(key);
				if (lock == null) {
					lock = new Serializable() {
						private static final long serialVersionUID = 255956860617836425L;
					};
					LOCAL_LOCKS.put(key, lock);
				}
			}
		}
		return lock;
	}

}
